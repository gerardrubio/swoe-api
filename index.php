<?php

error_reporting(E_ALL);

include __DIR__ . '/vendor/autoload.php';

use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\DI\FactoryDefault;
use Phalcon\Http\Response;
use Swoe\Exceptions\DocumentNotFoundException;
use Swoe\Models\Ajax\AjaxResponse;
use Swoe\Models\Cities\Cities;
use Swoe\Models\Devices\Devices;
use Swoe\Models\Dimensions\Dimensions;
use Swoe\Models\Graph\GraphItem;
use Swoe\Models\Protocols\Protocols;
use Swoe\Models\Readings\Readings;
use Swoe\Models\StatusInterface;

$debug = new \Phalcon\Debug();
$debug->listen(true, true);

$di = new FactoryDefault();

$di->set('mongo', function() {
    $mongo = new MongoClient();
    return $mongo->selectDB("swoe");
}, true);

$di->set('collectionManager', function(){
    return new Phalcon\Mvc\Collection\Manager();
}, true);

$app = new Micro($di);

//define the routes here
const EARTH_RADIUS = 6371.0072;
const GOOGLE_GEOLOCATION_API_KEY = 'AIzaSyDeLb9YRXr1FXue2HPYu0kBXBG_e1DoLFI';

$app->response->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, HEAD');
#$app->response->setHeader('Access-Control-Allow-Origin', $app->request->getHeader('Origin'));
$app->response->setHeader('Access-Control-Allow-Credentials', 'true');
$app->response->setHeader('Access-Control-Allow-Headers', "origin, x-requested-with, content-type");
$app->response->setHeader('Access-Control-Max-Age', '86400');

$app->get('/search/{latitude}/{longitude}[/]?{radius:[0-9]*}', function($latitude = 41.383333, $longitude = 2.183333, $radius = 1.0) use ($app) {

    $radius = empty($radius) ? 1.0 : $radius;
    $devices = Devices::findAroundPoint($latitude, $longitude, $radius);

    $response = new \Swoe\Models\Ajax\AjaxResponse();
    $response->data = $devices;
    return $app->response->setJsonContent($response);
});

$app->get('/search/reverse/{address}[/]?{radius:[0-9]*}', function($address, $radius = 1.0) use ($app) {
    $radius = empty($radius) ? 1.0 : floatval($radius);
    $query = "https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=" . GOOGLE_GEOLOCATION_API_KEY;
    $ch = curl_init($query);
    $options = [
        CURLOPT_RETURNTRANSFER => true,
    ];
    curl_setopt_array($ch, $options);
    $data = curl_exec($ch);
    $json_data = json_decode($data);

    if ($json_data && $json_data->status === 'OK')
    {
        $reversed = $json_data->results[0];
        $latitude = $reversed->geometry->location->lat;
        $longitude = $reversed->geometry->location->lng;
        return $app->response->redirect("search/$latitude/$longitude/$radius")->sendHeaders();
    }

    $response = new AjaxResponse();
    $response->status = false;
    $response->errors = ['Could not reverse geocode the given address'];
    return $app->response->setJsonContent($response);
});

$app->get('/graph/{latitude}/{longitude}[/]?{radius:[0-9]*}', function($latitude, $longitude, $radius = EARTH_RADIUS) use ($app) {
    $radius = empty($radius) ? EARTH_RADIUS : floatval($radius);
    error_log("/graph/{$latitude}/{$longitude}/{$radius}");
    $devices = Devices::findAroundPoint(floatval($latitude), floatval($longitude), $radius, [
        'location.coordinates' => 1
    ]);
error_log('Devices to aggregate: ' . count($devices));
    $results = [];
    $dimensions = Dimensions::getDefault();
    error_log('Dimension to match: ' . $dimensions->matcher);
    $protocolId = $dimensions->protocol;
    $dimensionMatcher = $dimensions->aggregationQuery();

    $minDate = new \DateTime();
    $minDate->modify('first day of last month');
    $minDateTimestamp = $minDate->getTimestamp();
    $maxDate = new \DateTime();
    $maxDate->modify('last day of this month');
    $maxDateTimestamp = $maxDate->getTimestamp();

    error_log("between $minDateTimestamp and $maxDateTimestamp");

    $max = 0;
    foreach ($devices as $device) {

        $graph = new GraphItem();

        $matcher = [
            'device' => $device->_id->__toString(),
            'protocol' => $protocolId,
            'timestamp' => [
                '$gte' => $minDateTimestamp,
                '$lte' => $maxDateTimestamp
            ],
        ];
        $matcher = array_merge($matcher, $dimensionMatcher);

        $readings = Readings::aggregate([
            [
                '$match' => $matcher,
            ],
            [
                '$group' => [
                    '_id' => '$device',
                    'avg' => [
                        '$avg' => '$data.f'
                    ]
                ]
            ]
        ]);

        if (!isset($readings['result'][0]))
            continue;

        $graph->id = $device->_id;
        $graph->location = $device->location;
        $graph->value = $readings['result'][0]['avg'];
        $max = max($max, $graph->value);
        $results[] = $graph;
    }

    $response = new AjaxResponse();
    $response->data = [
        'devices' => $results,
        'max' => $max
    ];

    return $app->response->setJsonContent($response);
});

$app->get('/graph/{latitude}/{longitude}/{radius}/{from}/{until}/{step_unit}', function($latitude, $longitude, $radius = EARTH_RADIUS, $from = '', $until = '', $step_unit = '') use ($app) {
    $devices = Devices::findAroundPoint(floatval($latitude), floatval($longitude), floatval($radius));

    $response = new AjaxResponse();
    $response->data = $devices;
    return $app->response->setJsonContent($response);
});

$app->get('/device/{id}', function($id) use ($app) {
    $response = new AjaxResponse();

    if (!MongoId::isValid($id)) {
        $response->status = false;
        $response->errors = ['Bad request'];
        $app->response->setStatusCode(400, 'Bad Request');
    }

    try {
        $device = Devices::findActiveById($id);
        if (empty($device))
            throw new DocumentNotFoundException;
    } catch (\MongoException $e) {
        $app->response->setStatusCode(404);
        $response->status = false;
        $response->errors = ['Device not found'];
        return $app->response->setJsonContent($response);
    } catch (DocumentNotFoundException $e) {
        $app->response->setStatusCode(404);
        $response->status = false;
        $response->errors = ['Device not found'];
        return $app->response->setJsonContent($response);
    }

    $response->data = $device;
    return $app->response->setJsonContent($response);
});

$app->post('/device/new', function() use ($app) {
    $new_device = Devices::fromDecodedJson($app->request->getJsonRawBody());

    //Create a response
    $response = new AjaxResponse();

    if (!is_string($new_device->name) || empty($new_device->name)) {
        $app->response->setStatusCode(400, 'Bad Request');
        $response->status = false;
        $response->errors[] = 'Device name must be a string';
        return $app->response->setJsonContent($response);
    }

    if (!filter_var(trim($new_device->ip), FILTER_VALIDATE_IP)) {
        $app->response->setStatusCode(400, 'Bad Request');
        $response->status = false;
        $response->errors[] = 'Device IP must be a valid IP address';
        return $app->response->setJsonContent($response);
    }

    if (!filter_var(trim($new_device->location->coordinates->latitude), FILTER_VALIDATE_FLOAT)) {
        $app->response->setStatusCode(400, 'Bad Request');
        $response->status = false;
        $response->errors[] = 'Device latitude must be a valid latitude value (float)';
        return $app->response->setJsonContent($response);
    }

    if (!filter_var(trim($new_device->location->coordinates->longitude), FILTER_VALIDATE_FLOAT)) {
        $app->response->setStatusCode(400, 'Bad Request');
        $response->status = false;
        $response->errors[] = 'Device longitude must be a valid longitude value (float)';
        return $app->response->setJsonContent($response);
    }

    $new_device->readings = [];
    $new_device->updated = null;
    $new_device->deleted = null;

    //Check if the insertion was successful
    if (empty($response->errors) && $new_device->save() == true) {
        //Change the HTTP status
        $app->response->setStatusCode(201, "Created");
        $response->data = $new_device->getId();
        return $app->response->setJsonContent($response);
    }

    //Change the HTTP status
    $app->response->setStatusCode(409, "Conflict");
    $response->status = false;
    //Send errors to the client
    foreach ($new_device->getMessages() as $message) {
        $response->errors[] = $message->getMessage();
    }

    return $app->response->setJsonContent($response);
});

$app->put('/device/{id}', function($id) use ($app) {
    $update = Devices::fromDecodedJson($app->request->getJsonRawBody());

    $response = new AjaxResponse();

    $id = filter_var(trim($id), FILTER_SANITIZE_STRING);
    if (!MongoId::isValid($id)) {
        $app->response->setStatusCode(400, 'Bad Request');
        $response->status = false;
        $response->errors[] = 'Bad Request';
        return $app->response->setJsonContent($response);
    }

    try {
        $device = Devices::findActiveById($id);
        if (empty($device))
            throw new DocumentNotFoundException;
    } catch (\MongoException $e) {
        $app->response->setStatusCode(404, 'Not found');
        $response->status = false;
        $response->errors[] = 'Device not found';
        return $app->response->setJsonContent($response);
    } catch (DocumentNotFoundException $e) {
        $app->response->setStatusCode(404, 'Not found');
        $response->status = false;
        $response->errors[] = 'Device not found';
        return $app->response->setJsonContent($response);
    }

    $device->sanitize($update);
    $device->updated = new MongoDate(time());

    //Check if the insertion was successful
    if ($device->save() == true) {
        $app->response->setStatusCode(202, "Accepted");
        return $app->response->setJsonContent($response);
    }

    $app->response->setStatusCode(409, "Conflict");
    $response->status = false;
    //Send errors to the client
    foreach ($device->getMessages() as $message) {
        $response->errors[] = $message->getMessage();
    }

    return $app->response->setJsonContent($response);
});

$app->delete('/device/{id}', function($id) use ($app) {
    $id = filter_var(trim($id), FILTER_SANITIZE_STRING);

    $response = new AjaxResponse();

    if (!MongoId::isValid($id)) {
        $app->response->setStatusCode(400, 'Bad Request');
        $response->status = false;
        $response->errors[] = 'Bad request';
        return $app->response->setJsonContent($response);
    }

    try {
        $device = Devices::findActiveById($id);
        if (empty($device))
            throw new DocumentNotFoundException;
    } catch (\MongoException $e) {
        $app->response->setStatusCode(404);
        $response->status = false;
        $response->errors[] = 'Bad request';
        return $app->response->setJsonContent($response);
    } catch (DocumentNotFoundException $e) {
        $app->response->setStatusCode(404);
        $response->status = false;
        $response->errors[] = 'Not found';
        return $app->response->setJsonContent($response);
    }

    $device->deleted = new MongoDate(time());
    //Check if the insertion was successful
    if ($device->save() === true) {
        $app->response->setStatusCode(202, "Accepted");
        $response->status = true;
        return $app->response->setJsonContent($response);
    }

    $app->response->setStatusCode(409, "Conflict");
    $response->status = false;
    $response->errors[] = 'Conflict';
    return $app->response->setJsonContent($response);
});

$app->post('/device/reading/{id}', function($id) use ($app) {
    $rawReading = $app->request->getJsonRawBody();

    $response = new AjaxResponse();

    $id = filter_var(trim($id), FILTER_SANITIZE_STRING);
    if (!MongoId::isValid($id)) {
        $app->response->setStatusCode(400, 'Bad Request');
        $response->status = false;
        $response->errors[] = 'Bad request';
        return $app->response->setJsonContent($response);
    }

    try {
        $device = Devices::findActiveById($id);
    } catch (\MongoException $e) {
        $app->response->setStatusCode(404, 'Not found');
    } catch (DocumentNotFoundException $e) {
        $response->status = false;
        $response->errors[] = 'Not found';
        return $app->response->setJsonContent($response);
    }

    try {
        $protocol = Protocols::getDefault();
    } catch (\MongoException $e) {
        $app->response->setStatusCode(404, 'Not found');
    } catch (DocumentNotFoundException $e) {
        $response->status = false;
        $response->errors[] = 'Not found';
        return $app->response->setJsonContent($response);
    }

    $reading = new Readings();
    $reading->data = $rawReading;
    $reading->device = $id;
    $reading->timestamp = new MongoDate($reading->timestamp);
    $reading->protocol = $protocol[0]->_id->__toString();
    if ($reading->save() === true) {
        //Change the HTTP status
        $app->response->setStatusCode(201, "Created");
        return $app->response->setJsonContent($response);
    }

    $app->response->setStatusCode(409, "Conflict");
    $response->status = false;
    //Send errors to the client
    foreach ($reading->getMessages() as $message) {
        $response->errors[] = $message->getMessage();
    }

    return $app->response->setJsonContent($response);
});

$app->get('/mock/update[/]?{page}', function($page = -1) use ($app) {
    ini_set('max_execution_time', 300);
    $response = new AjaxResponse();
    $response->data['readings'] = 0;
    if ($page >= 0)
        $devices = Devices::findBy([], 10000, intval($page, 10) * 10000);
    else
        $devices = Devices::findBy();

    foreach($devices as $device) {
        $baseValue = mt_rand(50, 225);
        $deviceId = $device->_id->__toString();
        $protocol = $device->protocol;
        for ($i = 0 ; $i < 12; $i++) {
            $reading = new Readings();
            $reading->device = $deviceId;
            $reading->protocol = $protocol;
            $reading->status = StatusInterface::ACTIVE;
            $reading->timestamp = time() - ($i / 12) * mt_rand(0, 1) * 31536000;
            $reading->data = [
                'a' => 1, //Electricidad
                'b' => 0,
                'c' => 1, //Potencia Activa QI+QIV
                'd' => 7, //Valor Instantaneo
                'e' => 0, //Total
                'f' => max($baseValue + mt_rand(-25, 25), 0),
            ];

            if (!$reading->save()) {
                $response->status = false;
                $response->errors[] = 'Could not save reading for device ' . $deviceId;
            } else {
                $response->data['readings']++;
            }
        }
    }

    if ($response->status) {
        $app->response->setStatusCode(202);
    } else {
        $app->response->setStatusCode(409);
    }

    return $app->response->setJsonContent($response);
});

$app->get('/mock/subscribe', function() use ($app) {
    ini_set('max_execution_time', 300);
    $response = new AjaxResponse();

    $robotId = '55df58b83f2e1ab32f45a36b';
    try {
        $defaultProtocol = Protocols::getDefault();
    } catch (DocumentNotFoundException $e) {
        $app->response->setStatusCode(404, 'Default protocol not found');
        $response->status = false;
        $response->errors[] = 'Default protocol not found';
        return $app->response->setJsonContent($response);
    }

    $protocolId = $defaultProtocol->_id->__toString();
    $cities = Cities::find();

    $minLatPrecision = 0.0003;
    $minLonPrecision = 0.0001;

    $response->data['minLatPrecision'] = $minLatPrecision;
    $response->data['minLonPrecision'] = $minLonPrecision;

    $response->data['devices'] = 0;
    $technologyReach = 0.01;
    $avgPeoplePerHousehold = 3;
    $degtorad = pi() / 180.0;

    foreach($cities as $city) {
        $devicesPerCity = floor($city->population * $technologyReach / $avgPeoplePerHousehold);

        for($i = 0; $i < $devicesPerCity; $i++) {
            $device = new Devices();
            $device->address = '';
            $device->description = 'Device created by ROBOT';
            $device->ip = long2ip(mt_rand());
            $rad = mt_rand(0,  100);
            $device->location = [
                'type' => 'Point',
                'coordinates' => [
                    $city->location['coordinates'][1] + cos(mt_rand(0, 360) * $degtorad) * $minLonPrecision * $rad,
                    $city->location['coordinates'][0] + sin(mt_rand(0, 360) * $degtorad) * $minLatPrecision * $rad,
                ]
            ];
            $device->name = sprintf('%s (%s - %s)', $city->city, $city->postalcode, $city->province);
            $device->owner = $robotId;
            $device->protocol = $protocolId;
            $device->status = StatusInterface::INACTIVE;
            $device->updated = time();

            if (!$device->save()) {
                $response->status = false;
                $response->errors[] = 'Could not subscribe a device @ ' . $device->location['coordinates'][0] . ',' . $device->location['coordinates'][0];
            } else {
                $response->data['devices']++;
            }
        }
    }

    if ($response->status) {
        $app->response->setStatusCode(201);
    } else {
        $app->response->setStatusCode(409);
    }

    return $app->response->setJsonContent($response);
});

$app->handle();
